@component('mail::message')
<h2>Hi Ryan,</h2>
<br>
{{$notification->title}}
<br>
@component('mail::button', ['url' => 'https://rldwebshop.xyz/auth/login' ])
Login to view your notification
@endcomponent
<br>
<br>
Thanks,<br>
{{ config('app.name') }}
@endcomponent

@component('mail::message')
<h1>Whats Up !</h1>
<br>
I recently started a new interesting project.
<br>
You may check it out just by clicking the button below.
<br>
@component('mail::button', ['url' => env('APP_LINK').'/works/'.$project->id ])
View Project
@endcomponent
<br>
<br>
Thanks,<br>
{{ config('app.name') }}
@endcomponent

@component('mail::message')
<h1>Whats Up !</h1>
<br>
I recently published a new article
<br>
You may check it out just by clicking the button below.
<br>
@component('mail::button', ['url' => env('APP_LINK').'/blog/'.$article->slug ])
View article
@endcomponent
<br><br>
Thanks,<br>
{{ config('app.name') }}
@endcomponent

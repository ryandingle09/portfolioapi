<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('prefix');
            $table->string('heading')->nullable();
            $table->text('heading_description')->nullable();
            $table->text('subscribe_description')->nullable();
            $table->text('header_image')->nullable();
            $table->text('package_image')->nullable();
            //$table->text('subscribe_image')->nullable();
            $table->string('heading_background_color')->nullable();
            $table->string('services_background_color')->nullable();
            $table->string('package_background_color')->nullable();
            $table->string('title')->nullable();
            $table->text('image')->nullable();
            $table->text('description')->nullable();
            $table->string('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_contents');
    }
}

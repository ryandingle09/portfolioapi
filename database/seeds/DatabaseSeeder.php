<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(SiteMetasTablesSeeder::class);
        $this->call(SocialsTableSeeder::class);
        //$this->call(BlogsTableSeeder::class);
        //$this->call(ProjectsTableSeeder::class);
        //$this->call(CategoryTableSeeder::class);
        //$this->call(TagsTableSeeder::class);
        //$this->call(NotificationsTableSeeder::class);
        //$this->call(SiteMetaTableSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' 		=>  'Ryan Dingle',
        	'firstname' =>  'Ryan',
        	'lastname' 	=>  'Dingle',
        	'age' 		=>  '24',
        	'birthdate' => 	'1993-04-09',
        	'address' 	=>  'Bulacan, Philippines',
        	'usertype' 	=>  'admin',
        	'email' 	=>  'ryandingle09@gmail.com',
        	'password' 	=>  Hash::make('ryan040993'),
            'image'     => 'https://scontent.fmnl3-1.fna.fbcdn.net/v/t1.0-9/12803086_957811210980712_4585720019932482305_n.jpg?oh=2f1d1e5d528cef996162270e795027e9&oe=5A150DF1',
            'created_at'=> date('Y-m-d') 
       	]);
    }
}

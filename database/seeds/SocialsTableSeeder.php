<?php

use Illuminate\Database\Seeder;

class SocialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('socials')->insert([
        	'name' 	=> 'facebook', 
        	'url' 	=> 'https://facebook.com/rldwesbhop',
		]);
		DB::table('socials')->insert([
			'name' 	=> 'twitter',
    		'url'  	=> 'https://twitter.com/ryandingle09'
		]);
		DB::table('socials')->insert([
			'name' 	=> 'linkedin',
    		'url' 	=> 'https://linkedin.com/in/ryandingle'
		]);
    }
}

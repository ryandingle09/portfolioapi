<?php

use Illuminate\Database\Seeder;

class SiteMetasTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('sitemetas')->insert([
        	'title' 		=>  'RLD Webshop',
        	'owner' 		=>  'Ryan Dingle',
        	'email' 		=>  'ryandingle09@gmail.com',
        	'tagline'   	=>  'The simplest things are the most beautiful',
        	'contact' 		=> 	'+639065601556',
        	'description' 	=>  'Ryan Dingle Personal Portfolio Website',
            'image'         =>  'https://scontent.fmnl3-1.fna.fbcdn.net/v/t1.0-9/12803086_957811210980712_4585720019932482305_n.jpg?oh=2f1d1e5d528cef996162270e795027e9&oe=5A150DF1',
        	'location' 		=>  'Metro Manila, Philippines (PH)',
        	'created_at' 	=> 	date('Y-m-d m:i:s'),
       	]);
    }
}

-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 25, 2018 at 09:56 PM
-- Server version: 5.7.19-0ubuntu0.17.04.1
-- PHP Version: 7.1.10-1+ubuntu17.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
use portfolioapi;
--
-- Database: `portfolioapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_contents`
--

CREATE TABLE `cms_contents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `prefix` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heading` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heading_description` text COLLATE utf8mb4_unicode_ci,
  `subscribe_description` text COLLATE utf8mb4_unicode_ci,
  `header_image` text COLLATE utf8mb4_unicode_ci,
  `package_image` text COLLATE utf8mb4_unicode_ci,
  `heading_background_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `services_background_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_background_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_contents`
--

INSERT INTO `cms_contents` (`id`, `prefix`, `heading`, `heading_description`, `subscribe_description`, `header_image`, `package_image`, `heading_background_color`, `services_background_color`, `package_background_color`, `title`, `image`, `description`, `date`, `created_at`, `updated_at`) VALUES
(5, 'home', 'We KNOW what we are doing!', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Please subscribe to our newsletter so you can learn the updates for new products before anyone else. Also we will spam your mailbox.', NULL, NULL, '#999', '#999', '#888', NULL, NULL, NULL, NULL, '2018-02-20 23:28:21', '2018-02-25 05:41:27'),
(9, 'portfolio', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WEB ARTISANS FOR YOUR PROJECTS!', NULL, 'I love challenging projects that makes me think out of the box. Creating business logic, analyzing data, data testing and other cool activities of being a full stack developer makes my self grow in this kind of work field. I would love to deliver quality works to all my clients and hearing thier feedback on my work makes me feel great and do much better on my future projects.', NULL, '2018-02-21 01:25:44', '2018-02-21 18:56:47'),
(10, 'blog', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WELCOME TO MY BLOG', NULL, 'Read some interesting article, news, and anything interesting things around the world here in my blog.', NULL, '2018-02-21 02:11:41', '2018-02-21 18:57:50'),
(15, 'about', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ABOUT ME', NULL, 'Know more about my skills and background history. ', NULL, '2018-02-21 03:17:13', '2018-02-21 18:58:11'),
(16, 'contact', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'FEEL FREE TO DROP ME A LINE', NULL, 'I would love to hear from you. Please drop me a line and let me know what you think. I will get back to you as soon as i can.', NULL, '2018-02-21 03:23:59', '2018-02-21 18:40:18'),
(24, 'history', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ama Computer Learning Center ( 2yr Vocational Diploma Course )', NULL, 'Taking first a 2 yr course of programming to see if this is fun and exciting.', '2010 - 2012', '2018-02-25 04:15:47', '2018-02-25 04:15:47'),
(25, 'history', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Vigattin Inc. - Full Stack Web Developer', NULL, 'I started here as an OJT student then after i finish my 2 yr course of programming they absorbed me as a regular part timer full stack php developer because i want to continue my 2 yr course to a bachelor degree 4 yr course at AMA UNIVERSITY.', '2013 - 2014', '2018-02-25 04:18:45', '2018-02-25 04:28:41'),
(26, 'history', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AMA UNIVERSITY', NULL, 'While having my part time work i also continue my 2 yr course here as Bachelor of science in computer science (BSCS) major in Software development where in i finish and graduated on 2014 but i got to marched on stage on 2015. I experience a lot of fun studying here, a lot of friends and also a lot of freelance thesis projects i did for the other students and at the same time i was earning while studying.', '2012 - 2015', '2018-02-25 04:22:39', '2018-02-25 04:22:39'),
(27, 'history', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Adobohost Inc.', NULL, 'This is my first full time work as a full stack web developer. I learned a lot here because of my role as a full stack developer.', 'feb 2015 to august 2015', '2018-02-25 04:24:48', '2018-02-25 04:24:48'),
(28, 'history', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Asiagate Networks Inc. - Php Developer (Project Based)', NULL, 'I started to work to other company to explore other exciting projects so i started to work to this company for a project basis for 6 months and a just finished for 3 months then a want to find another project to the other company to explore a lot of exciting projects.', 'May 2016 - August 2016', '2018-02-25 04:27:50', '2018-02-25 04:27:50'),
(29, 'history', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Health.Links (Full Stack Developer - Home Base)', NULL, 'This company is based in Jeddah, Saudi Arabia. I grab this work for a project basis where in we work as a team of 3 developers developing an Health Operation System for Hospital Clients of this company.', 'Sept 2016 - March 2017', '2018-02-25 04:38:46', '2018-02-25 04:38:46'),
(30, 'history', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Nityo Infotech (Laravel Php Developer)', NULL, 'This is my present work as a laravel php developer but i do support, server, frontend and backend to the projects that given to me. But my main project here is the payroll system of the client wherein i am the one who handled everything on this system.', 'October 2017 - present', '2018-02-25 04:42:25', '2018-02-25 04:42:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_contents`
--
ALTER TABLE `cms_contents`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_contents`
--
ALTER TABLE `cms_contents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

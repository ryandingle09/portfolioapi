-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 27, 2018 at 10:37 AM
-- Server version: 5.7.9-log
-- PHP Version: 7.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
use `portfolioapi`;
--
-- Database: `portfolioapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_08_03_062821_create_projects_table', 1),
(4, '2017_08_03_062832_create_blogs_table', 1),
(5, '2017_08_03_062840_create_tags_table', 1),
(7, '2017_08_03_062954_create_sitemetas_table', 1),
(8, '2017_08_03_074635_create_categories_table', 1),
(9, '2017_08_03_080931_create_likes_table', 1),
(10, '2017_08_05_130345_create_socials_table', 1),
(11, '2017_08_05_131209_create_item_categories_table', 1),
(12, '2017_08_05_131221_create_item_tags_table', 1),
(13, '2018_02_21_003749_create_cms_contents_table', 2),
(20, '2017_08_03_062924_create_notifications_table', 3),
(22, '2018_02_26_030032_create_messages_table', 3),
(24, '2018_02_26_025556_create_subscribers_table', 4),
(25, '2018_02_26_112141_create_jobs_table', 5),
(26, '2018_02_27_074950_create_visitors_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `title`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-25 23:37:31', '2018-02-25 23:37:31'),
(2, 'New subscriber ryandingles09@gmail.com', 'subscriber', 0, '2018-02-26 00:02:19', '2018-02-26 00:02:19'),
(3, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-26 00:14:03', '2018-02-26 00:14:03'),
(4, 'New message from Ryan', 'message', 0, '2018-02-26 00:22:43', '2018-02-26 00:22:43'),
(5, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-26 01:24:36', '2018-02-26 01:24:36'),
(6, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-26 01:26:08', '2018-02-26 01:26:08'),
(7, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-26 01:27:14', '2018-02-26 01:27:14'),
(8, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-26 01:29:26', '2018-02-26 01:29:26'),
(9, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-26 01:30:22', '2018-02-26 01:30:22'),
(10, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-26 02:14:12', '2018-02-26 02:14:12'),
(11, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-26 02:19:03', '2018-02-26 02:19:03'),
(12, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-26 02:29:56', '2018-02-26 02:29:56'),
(13, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-26 17:55:56', '2018-02-26 17:55:56'),
(14, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-26 17:58:16', '2018-02-26 17:58:16'),
(15, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-26 18:00:08', '2018-02-26 18:00:08'),
(16, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-26 18:00:40', '2018-02-26 18:00:40'),
(17, 'New message from Ryan Dingle', 'message', 0, '2018-02-26 18:13:51', '2018-02-26 18:13:51'),
(18, 'New message from Ryan Dingle', 'message', 0, '2018-02-26 18:24:04', '2018-02-26 18:24:04'),
(19, 'New message from Ryan Dingle', 'message', 0, '2018-02-26 18:24:44', '2018-02-26 18:24:44'),
(20, 'New message from Ryan Dingle', 'message', 0, '2018-02-26 18:25:19', '2018-02-26 18:25:19'),
(21, 'New message from Ryan Dingle', 'message', 0, '2018-02-26 18:33:02', '2018-02-26 18:33:02'),
(22, 'New subscriber a@gmail.com', 'subscriber', 0, '2018-02-26 18:47:58', '2018-02-26 18:47:58'),
(23, 'New subscriber a@gmail.coms', 'subscriber', 0, '2018-02-26 19:04:23', '2018-02-26 19:04:23'),
(24, 'New subscriber hi@gmail.coms', 'subscriber', 0, '2018-02-26 19:29:22', '2018-02-26 19:29:22'),
(25, 'New subscriber as@gmail.com', 'subscriber', 0, '2018-02-26 22:01:28', '2018-02-26 22:01:28'),
(26, 'New subscriber as@gmail.coms', 'subscriber', 0, '2018-02-26 23:08:24', '2018-02-26 23:08:24'),
(27, 'New subscriber rldwebshop@gmail.com', 'subscriber', 0, '2018-02-26 23:30:47', '2018-02-26 23:30:47'),
(28, 'New subscriber ryandingle09@gmail.com', 'subscriber', 0, '2018-02-26 23:38:39', '2018-02-26 23:38:39'),
(29, 'You have a new website visitor', 'Visitor', 0, '2018-02-27 02:16:43', '2018-02-27 02:16:43'),
(30, 'You have a new website visitor', 'Visitor', 0, '2018-02-27 02:19:44', '2018-02-27 02:19:44');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--
DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE `subscribers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--
DROP TABLE IF EXISTS `visitors`;
CREATE TABLE `visitors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `browser` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subscribers_email_unique` (`email`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=530;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

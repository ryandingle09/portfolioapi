<?php

//header("Access-Control-Allow-Origin", "*");//apache
header("Access-Control-Allow-Origin: *");//nginx
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT, PATCH");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, If-Modified-Since, Cache-Control, Pragma, x-xsrf-token, x-csrf-token, token");
header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");

use Illuminate\Http\Request;

use RLD\User, RLD\Social, RLD\Sitemeta;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group( ['prefix' => 'v1'], function () {

        /*JWT ROUTES FOR TOKEN REQUESTING ONLY*/
        Route::post('auth/register', 'UserController@register');
        Route::post('auth/login', 'UserController@login');
        Route::post('auth/logout', 'UserController@logout');

        /*SITE ROUTES*/
        Route::get('/site', 'SiteController@index');
        Route::get('/social', 'SocialController@index');

        /*USER ROUTES*/
        Route::get('/user/{id}', 'UserController@show');
        Route::get('/user/{id}/get', 'UserController@show');
        Route::post('/user/{id}/update', 'UserController@update');

        /*PROJECT ROUTES*/
        Route::post('project', 'ProjectController@index' );//list
        Route::get('project/{id}/get', 'ProjectController@show' );//item
        Route::get('project/{id}/edit', 'ProjectController@edit' );//edit
        Route::post('project/{id}/delete', 'ProjectController@destroy' );//delete
        Route::post('project/{id}/update', 'ProjectController@update' );//update
        Route::post('project/post', 'ProjectController@store');//insert
        Route::get('project/recent-post', 'ProjectController@recentPost' );//get 10 recent post

        /*BLOG ROUTES*/
        Route::post('blog', 'BLogController@index' );
        Route::get('blog/{id}/get', 'BLogController@show' );//item
        Route::get('blog/{slug}/show', 'BLogController@showBySlug' );//edit
        Route::get('blog/{id}/edit', 'BLogController@edit' );//edit
        Route::post('blog/{id}/delete', 'BLogController@destroy' );//delete
        Route::post('blog/{id}/update', 'BLogController@update' );//update
        Route::post('blog/post', 'BLogController@store' );//insert
        Route::get('blog/recent-post', 'BLogController@recentPost' );//get 10 recent post
        Route::get('blog/category/list', 'BLogController@categoryList' );//get categories for sidebar post
        Route::post('blog/{id}/post-by-category', 'BLogController@postByCategory' );

        /*TAG ROUTES*/
        Route::get('tag', 'TagController@index'); //get
        Route::post('tag/list', 'TagController@list'); //post
        Route::post('tag/post', 'TagController@store'); //post
        Route::get('tag/{id}/get', 'TagController@show' );//item
        Route::get('tag/{id}/edit', 'TagController@edit' );//edit
        Route::post('tag/{id}/delete', 'TagController@destroy' );//delete
        Route::post('tag/{id}/update', 'TagController@update' );//update

        /*CATEGORY ROUTES*/
        Route::get('category', 'CategoryController@index'); //get
        Route::post('category/list', 'CategoryController@list'); //post
        Route::post('category/post', 'CategoryController@store'); //post
        Route::get('category/{id}/get', 'CategoryController@show' );//item
        Route::get('category/{id}/edit', 'CategoryController@edit' );//edit
        Route::post('category/{id}/delete', 'CategoryController@destroy' );//delete
        Route::post('category/{id}/update', 'CategoryController@update' );//update

        /*SITE ROUTES*/
        Route::post('site/post', 'SiteController@store');
        Route::post('site/{id}/update', 'SiteController@update');

        /*DASHBOARD COUNTER ROUTES*/
        Route::get('counters', 'CounterController@index' );//item counts

        /*CMS ROUTES*/
        Route::post('cms/{page}/list', 'CmsController@index' );//post  
        Route::post('cms/post', 'CmsController@store' );//post    
        Route::get('cms/{id}/get', 'CmsController@show' );//get 
        Route::get('cms/{page}/pagecms', 'CmsController@pagecms' );//get
        Route::get('cms/{id}/edit', 'CmsController@edit' );//get
        Route::post('cms/{id}/update', 'CmsController@update' );//post
        Route::delete('cms/{id}/delete', 'CmsController@destroy' );//post

        /*HOME ROUTES*/
        Route::post('subscriber/post', 'SubscriberController@store' );//post  

        /*ABOUT ROUTES*/
        Route::post('message/post', 'MessageController@store' );//post 

        /*NOTIFICATION ROUTES*/
        Route::post('notification', 'NotificationController@index' );//post 
        Route::post('notification/read', 'NotificationController@update' );//post 
        Route::post('notification/read-all', 'NotificationController@readAll' );//post 
        Route::get('notification/{type}/{id}/get', 'NotificationController@show' );//get 

        /*VISITOR ROUTES*/
        Route::post('visitor/post', 'VisitorController@store' );//post
        //Route::get('visitor/post', 'VisitorController@store' );//post    

    }
);

<?php

namespace RLD;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    protected $table = 'cms_contents';
}

<?php

namespace RLD\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use RLD\Mail\Article;
use Illuminate\Contracts\Mail\Mailer;

class SendArticleEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $article;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($article)
    {
        $this->article;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $arr = [];
        $subscribers = Subscriber::get();
        foreach($subscribers as $sub)
        {
            array_push($sub->email, $arr);
        }

        $mailer->to($arr)->send(new Work($this->article));
    }
}

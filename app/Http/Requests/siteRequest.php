<?php

namespace RLD\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class siteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;//false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required',
            'email'         => 'required|email',
            'tagline'       => 'required',
            'description'   => 'required',
            'contact'       => 'required|numeric',
            'location'      => 'required',
            'owner'         => 'required',
            'photo'         => 'image|mimes:png,sgv,jpg,jpeg,gif', 
        ];
    }
}

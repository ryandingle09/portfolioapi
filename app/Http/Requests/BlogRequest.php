<?php

namespace RLD\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;//false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required|unique:blogs|max:255',
            'slug'          => 'required|unique:blogs|max:255',
            'tag'           => 'required',
            'category'      => 'required',
            'body'          => 'required',
            'photo'         => 'required|image|mimes:jpeg,jpg,png,svg,gif',
            'status'        => 'required|max:11',
        ];
    }
}

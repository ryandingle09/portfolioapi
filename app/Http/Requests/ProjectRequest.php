<?php

namespace RLD\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;//false
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'title'         => 'required|unique:projects|max:255',
            'slug'          => 'required|unique:projects|max:255',
            'link'          => 'url|nullable',
            'photo'         => 'image|mimes:jpeg,jpg,png,svg,gif|nullable',
            'description'   => 'required',
            'category'      => 'required',
            'status'        => 'required|max:11',
        ];
    }
}

<?php

namespace RLD\Http\Controllers;

use Illuminate\Http\Request;
use RLD\Http\Requests\ProjectRequest;
use Illuminate\Support\Facades\Validator;
use RLD\Project,
    RLD\Item_category,
    RLD\Jobs\SendProjectEmail;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->input('value') != '')
        {
            if($request->input('offset') == 0)
            {
                $data = Project::where(
                    'id', 'like', '%'.$request->input('value').'%')
                    ->orWhere('title', 'like', '%'.$request->input('value').'%')
                    ->orWhere('description', 'like', '%'.$request->input('value').'%')
                    ->orWhere('status', 'like', '%'.$request->input('value').'%')
                    ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                    ->limit($request->input('per_page'))
                    ->orderBy('id', 'DESC')
                    ->get();
            }
            else
            {
                $data = Project::where(
                    'id', 'like', '%'.$request->input('value').'%')
                    ->orWhere('title', 'like', '%'.$request->input('value').'%')
                    ->orWhere('description', 'like', '%'.$request->input('value').'%')
                    ->orWhere('status', 'like', '%'.$request->input('value').'%')
                    ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                    ->limit($request->input('per_page'))
                    ->offset($request->input('offset'))
                    ->orderBy('id', 'DESC')
                    ->get();
            }

            $count = count(Project::where(
                'id', 'like', '%'.$request->input('value').'%')
                ->orWhere('title', 'like', '%'.$request->input('value').'%')
                ->orWhere('description', 'like', '%'.$request->input('value').'%')
                ->orWhere('status', 'like', '%'.$request->input('value').'%')
                ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                ->orderBy('id', 'DESC')
                ->get()
            );
            
        }
        else
        {
            if($request->input('offset') == 0)
            {
                $data = Project::limit($request->input('per_page'))
                    ->orderBy('id', 'DESC')
                    ->get();
            }
            else
            {
                $data = Project::limit($request->input('per_page'))
                    ->orderBy('id', 'DESC')
                    ->offset($request->input('offset'))
                    ->get();
            }

            $count = count(Project::get());
        }

        if(!$data) return Response()->json(['message', 'Error fetching records.'], 201);
        return Response()->json([['total' => $count, 'data' => $data]], 200);
    }

    public function recentPost() 
    {
        $data = Project::limit(10)->orderBy('id', 'DESC')->get();
        return Response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {
        $process    = new Project;

        if ($request->hasFile('photo') && $request->file('photo') !== '') 
        {
            $valid_img = Validator::make($request->all(), ['photo' => 'image|mimes:png,jpeg,jpg,svg,gif']);
            if($valid_img->fails())  return Response()->json($valid_img->errors('photo'), 422);
            else
            {
                $file   = $request->file('photo');
                $ext    =  $file->guessClientExtension();
                $path   = base_path().'/public/uploads/project/';
                $image  = time().''.sha1(bcrypt(time())).'.n.'.$ext;
                $photo  = $request->file('photo')->move($path, $image);
                $public = url('/').'/uploads/project/'.$image;
                $process->image         = $public;
            }
        }
        $process->title         = $request->input('title');
        $process->slug          = $request->input('slug');
        $process->description   = $request->input('description');
        $process->link          = $request->input('link');
        $process->status        = $request->input('status');
        $process->save();

        $insertedId = $process->id;

        $data = Project::find($insertedId);

        if(!$process) return Response()->json(['failed', 'Adding record failed.'], 201);

        $categories = explode(',',$request->input('category'));
        foreach($categories as $keys)
        {
            $category               = new Item_category;
            $category->category_id  = $keys;
            $category->article_id   = $insertedId;
            $category->save();
        }

        $job = (new SendProjectEmail($data));
        dispatch($job);

        return Response()->json(['success' => 'Successfully Added.'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $json = Project::find($id);
        if(!$json) return Response()->json(['message' => 'Unable to find item'], 201);
        return Response()->json([$json]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Project::find($id);
        $categories = Item_category::where('article_id',$id)->get();

        $c_ar = [];
        foreach ($categories as $value) $c_ar[] = ''.$value['category_id'].'';

        if(!$data) return Response()->json(['message' => 'Unable to find item'], 201);
        return Response()->json(['data' => $data, 'categories' => $c_ar], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $base_path          = base_path().'/public';
        $process            = Project::find($id);
        $unique             = '|unique:projects';

        if($process->slug == $request->input('slug') || $process->title == $request->input('title'))
        {
            $unique = '';
        }

        $rules = [
            'title'         => 'required|max:255'.$unique.'',
            'slug'          => 'required|max:255'.$unique.'',
            'link'          => 'url|nullable',
            'description'   => 'required',
            'status'        => 'required',
        ];

        $validation = Validator::make($request->all(), $rules);

        if($validation->fails()) return response()->json($validation->errors('title'), 422);

        if ($request->hasFile('photo') && $request->file('photo') !== '') 
        {
            $valid_img = Validator::make($request->all(), ['photo' => 'image|mimes:png,jpeg,jpg,svg,gif']);
            if($valid_img->fails())  return Response()->json($valid_img->errors('photo'), 422);
            else
            {
                $db_image       = str_replace(url('/'), $base_path, $process->image);
                if(file_exists($db_image)) unlink($db_image);

                $file           = $request->file('photo');
                $ext            =  $file->guessClientExtension();
                $path           = base_path().'/public/uploads/project/';
                $image          = time().''.sha1(bcrypt(time())).'.n.'.$ext;
                $photo          = $request->file('photo')->move($path, $image);
                $public         = url('/').'/uploads/project/'.$image;
                $process->image = $public;
            } 
        }

        $process->title         = $request->input('title');
        $process->slug          = $request->input('slug');
        $process->description   = $request->input('description');
        $process->link          = $request->input('link');
        $process->status        = $request->input('status');
        $process->save();

        $data = Project::find($id);

        if(!$process) return Response()->json(['failed', 'Updating record failed.'], 201);

        Item_category::where('article_id', $id)->delete();

        $categories    = explode(',',$request->input('category'));

        foreach($categories as $keys)
        {
            $category               = new Item_category;
            $category->category_id  = $keys;
            $category->article_id   = $id;
            $category->save();
        }

        return Response()->json(['success' => 'Successfully Updated.'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $base_path  = base_path().'/public';
        $process    = Project::find($id);
        Item_category::where('article_id',$id)->delete();

        $db_image = str_replace(url('/'), $base_path, $process->image);
        
        if(file_exists($db_image)) unlink($db_image);

        $process->delete();
        
        if(!$process) return Response()->json(['failed', 'Removing item error.'], 422);
        return Response()->json(['success' => 'Successfully Deleted.'], 200);
    }
}

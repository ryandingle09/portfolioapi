<?php

namespace RLD\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use RLD\Cms, RLD\Project, RLD\Sitemeta, RLD\Social;

class CmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $page)
    {
        $data = [];

        if($page == 'skill' || $page == 'service' || $page == 'history')
        {
            if($request->input('value') != '')
            {
                if($request->input('offset') == 0)
                {
                    $data = Cms::where(
                        'id', 'like', '%'.$request->input('value').'%')
                        ->where('prefix', $page)
                        ->orWhere('title', 'like', '%'.$request->input('value').'%')
                        ->orWhere('description', 'like', '%'.$request->input('value').'%')
                        ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                        ->limit($request->input('per_page'))
                        ->orderBy('id', 'DESC')
                        ->get();
                }
                else
                {
                    $data = Cms::where(
                        'id', 'like', '%'.$request->input('value').'%')
                        ->where('prefix', $page)
                        ->orWhere('title', 'like', '%'.$request->input('value').'%')
                        ->orWhere('description', 'like', '%'.$request->input('value').'%')
                        ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                        ->limit($request->input('per_page'))
                        ->offset($request->input('offset'))
                        ->orderBy('id', 'DESC')
                        ->get();
                }

                $count = sizeof(Cms::where(
                    'id', 'like', '%'.$request->input('value').'%')
                    ->where('prefix', $page)
                    ->orWhere('title', 'like', '%'.$request->input('value').'%')
                    ->orWhere('description', 'like', '%'.$request->input('value').'%')
                    ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                    ->orderBy('id', 'DESC')
                    ->get()
                );
                
            }
            else
            {
                if($request->input('offset') == 0)
                {
                    $data = Cms::limit($request->input('per_page'))
                        ->where('prefix', $page)
                        ->orderBy('id', 'DESC')
                        ->get();
                }
                else
                {
                    $data = Cms::limit($request->input('per_page'))
                        ->where('prefix', $page)
                        ->offset($request->input('offset'))
                        ->orderBy('id', 'DESC')
                        ->get();
                }

                $count = sizeof(Cms::where('prefix', $page)->get());
            }

            if(!$data) return Response()->json(['message', 'Error fetching records.'], 201);
            return Response()->json([['total' => $count, 'data' => $data]], 200);
        }
        else
        {
            $data = Cms::where('prefix', $page)->get();
            if(!$data) return Response()->json(['message', 'Error fetching records.'], 201);
            return Response()->json($data, 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $page       = $request->input('page');
        $process    = new Cms;
        $path       = base_path().'/public/uploads/cms/';

        if($page == 'home')
        {
            if($request->hasFile('header_image'))
            {
                $valid_img = Validator::make($request->all(), ['header_image' => 'image|mimes:png,jpg,jpeg,svg,gif']);
                if($valid_img->fails()) return Response()->json($valid_img->errors('header_image'), 422);
                else
                {
                    $file2      = $request->file('header_image');
                    $ext2       = $file2->guessClientExtension();
                    $image2     = time().''.sha1(bcrypt(time())).'.n.'.$ext2;
                    $photo2     = $request->file('header_image')->move($path, $image2);
                    $public2    = url('/').'/uploads/cms/'.$image2;
                    $process->header_image = $public2;
                }
            }
            if($request->hasFile('package_image'))
            {
                $valid_img = Validator::make($request->all(), ['package_image' => 'image|mimes:png,jpg,jpeg,svg,gif']);
                if($valid_img->fails()) return Response()->json($valid_img->errors('package_image'), 422);
                else
                {
                    $file3      = $request->file('package_image');
                    $ext3       = $file3->guessClientExtension();
                    $image3     = time().''.sha1(bcrypt(time())).'.n.'.$ext3;
                    $photo3     = $request->file('package_image')->move($path, $image3);
                    $public3    = url('/').'/uploads/cms/'.$image3;
                    $process->package_image = $public3;
                }
            }
            if($request->hasFile('subscribe_image'))
            {
                $valid_img = Validator::make($request->all(), ['subscribe_image' => 'image|mimes:png,jpg,jpeg,svg,gif']);
                if($valid_img->fails()) return Response()->json($valid_img->errors('subscribe_image'), 422);
                else
                {
                    $file4      = $request->file('subscribe_image');
                    $ext4       = $file4->guessClientExtension();
                    $image4     = time().''.sha1(bcrypt(time())).'.n.'.$ext4;
                    $photo4     = $request->file('subscribe_image')->move($path, $image4);
                    $public4    = url('/').'/uploads/cms/'.$image4;
                    $process->subscribe_image  = $public4;
                }
            }
            
            $process->heading                       = $request->input('heading');
            $process->heading_description           = $request->input('heading_description');
            $process->subscribe_description         = $request->input('subscribe_description');
            $process->heading_background_color      = $request->input('heading_background_color');
            $process->package_background_color      = $request->input('package_background_color');
            $process->services_background_color     = $request->input('heading_background_color');
        }
        else if($page == 'history')
        {
            $process->title         = $request->input('title');
            $process->date          = $request->input('date');
            $process->description   = $request->input('description');
        }
        else
        {
            if($page == 'service' || $page == 'skill')
            {
                $valid_img = Validator::make($request->all(), ['image' => 'required|image|mimes:png,jpg,jpeg,svg,gif']);
                if($valid_img->fails()) return Response()->json($valid_img->errors('image'), 422);
            }

            if($request->hasFile('image'))
            {
                $valid_img = Validator::make($request->all(), ['image' => 'image|mimes:png,jpg,jpeg,svg,gif']);
                if($valid_img->fails()) return Response()->json($valid_img->errors('image'), 422);
                else
                {
                    $file1      = $request->file('image');
                    $ext1       = $file1->guessClientExtension();
                    $image      = time().''.sha1(bcrypt(time())).'.n.'.$ext1;
                    $photo      = $request->file('image')->move($path, $image);
                    $public     = url('/').'/uploads/cms/'.$image;
                    $process->image         = $public;
                }
            }
            
            $process->title         = $request->input('title');
            $process->description   = $request->input('description');
        }
        
        $process->prefix = $page;
        $process->save();

        $id = $process->id;

        $data = Cms::find($id);

        if(!$process) return Response()->json(['failed' => 'Unable to process your post request.'], 422);
        return Response()->json(['success' => 'Successfuly Added.', 'data' => $data], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data       = Cms::find($id);

        if(!$data) return Response()->json(['message' => 'Unable to find item'], 201);
        return Response()->json([$data],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data       = Cms::find($id);

        if(!$data) return Response()->json(['message' => 'Unable to find item'], 201);
        return Response()->json([$data],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page       = $request->input('page');
        $process    = Cms::find($id);
        $path       = base_path().'/public/uploads/cms/';
        $base_path  = base_path().'/public';

        if($page == 'home')
        {
            if($request->hasFile('header_image'))
            {
                $valid_img = Validator::make($request->all(), ['header_image' => 'image|mimes:png,jpg,jpeg,svg,gif']);
                if($valid_img->fails()) return Response()->json($valid_img->errors('header_image'), 422);
                else
                {
                    $db_image = str_replace(url('/'), $base_path, $process->header_image);
                    if(file_exists($db_image)) unlink($db_image);

                    $file2      = $request->file('header_image');
                    $ext2       = $file2->guessClientExtension();
                    $image2     = time().''.sha1(bcrypt(time())).'.n.'.$ext2;
                    $photo2     = $request->file('header_image')->move($path, $image2);
                    $public2    = url('/').'/uploads/cms/'.$image2;
                    $process->header_image = $public2;
                }
            }
            if($request->hasFile('package_image'))
            {
                $valid_img = Validator::make($request->all(), ['package_image' => 'image|mimes:png,jpg,jpeg,svg,gif']);
                if($valid_img->fails()) return Response()->json($valid_img->errors('package_image'), 422);
                else
                {
                    $db_image = str_replace(url('/'), $base_path, $process->package_image);
                    if(file_exists($db_image)) unlink($db_image);

                    $file3      = $request->file('package_image');
                    $ext3       = $file3->guessClientExtension();
                    $image3     = time().''.sha1(bcrypt(time())).'.n.'.$ext3;
                    $photo3     = $request->file('package_image')->move($path, $image3);
                    $public3    = url('/').'/uploads/cms/'.$image3;
                    $process->package_image = $public3;
                }
            }
            if($request->hasFile('subscribe_image'))
            {
                $valid_img = Validator::make($request->all(), ['subscribe_image' => 'image|mimes:png,jpg,jpeg,svg,gif']);
                if($valid_img->fails()) return Response()->json($valid_img->errors('subscribe_image'), 422);
                else
                {
                    $db_image = str_replace(url('/'), $base_path, $process->subscribe_image);
                    if(file_exists($db_image)) unlink($db_image);

                    $file4      = $request->file('subscribe_image');
                    $ext4       = $file4->guessClientExtension();
                    $image4     = time().''.sha1(bcrypt(time())).'.n.'.$ext4;
                    $photo4     = $request->file('subscribe_image')->move($path, $image4);
                    $public4    = url('/').'/uploads/cms/'.$image4;
                    $process->subscribe_image  = $public4;
                }
            }
            
            $process->heading                       = $request->input('heading');
            $process->heading_description           = $request->input('heading_description');
            $process->subscribe_description         = $request->input('subscribe_description');
            $process->heading_background_color      = $request->input('heading_background_color');
            $process->package_background_color      = $request->input('package_background_color');
            $process->services_background_color     = $request->input('heading_background_color');
        }
        else if($page == 'history')
        {
            $process->title         = $request->input('title');
            $process->date          = $request->input('date');
            $process->description   = $request->input('description');
        }
        else
        {
            if($request->hasFile('image'))
            {
                $valid_img = Validator::make($request->all(), ['image' => 'image|mimes:png,jpg,jpeg,svg,gif']);
                if($valid_img->fails()) return Response()->json($valid_img->errors('image'), 422);
                else
                {
                    $db_image = str_replace(url('/'), $base_path, $process->image);
                    if(file_exists($db_image)) unlink($db_image);

                    $file1      = $request->file('image');
                    $ext1       = $file1->guessClientExtension();
                    $image      = time().''.sha1(bcrypt(time())).'.n.'.$ext1;
                    $photo      = $request->file('image')->move($path, $image);
                    $public     = url('/').'/uploads/cms/'.$image;
                    $process->image         = $public;
                }
            }
            
            $process->title         = $request->input('title');
            $process->description   = $request->input('description');
        }

        $process->save();

        $data = Cms::find($process->id);

        if(!$process) return Response()->json(['failed', 'Updating record failed.'], 201);
        return Response()->json(['success' => 'Successfuly Updated.', 'data' => $data], 200);
    }

    /**
     * Get specific cms page
     *
     * @param  string  $page
     * @return \Illuminate\Http\Response
     */
    public function pagecms($page)
    {
        $home       = Cms::where('prefix', $page)->get();
        $about      = Cms::where('prefix', 'about')->get();
        $blog       = Cms::where('prefix', 'blog')->get();
        $contact    = Cms::where('prefix', 'contact')->get();
        $portfolio  = Cms::where('prefix', 'portfolio')->get();
        $service    = Cms::where('prefix', 'service')->get();
        $skill      = Cms::where('prefix', 'skill')->get();
        $history    = Cms::where('prefix', 'history')->orderBy('id', 'DESC')->get();
        $project    = Project::limit(6)->orderBy('id', 'DESC')->get();
        $social     = Social::get();
        $site       = Sitemeta::get();

        return Response()->json([
            'home'      => $home, 
            'about'     => $about,
            'blog'      => $blog,
            'portfolio' => $portfolio,
            'project'   => $project,
            'contact'   => $contact,
            'service'   => $service, 
            'skill'     => $skill,
            'history'   => $history,
            'social'    => $social,
            'site'      => $site
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $process    = Cms::find($id);
        $base_path  = base_path().'/public'; 
        $db_image   = str_replace(url('/'), $base_path, $process->image);
        if(file_exists($db_image)) unlink($db_image);
        $process->delete();
        
        if(!$process) return Response()->json(['failed', 'Removing item error.'], 422);
        return Response()->json(['success' => 'Successfuly Deleted.'], 200);
    }
}

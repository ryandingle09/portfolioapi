<?php

namespace RLD\Http\Controllers;

use Illuminate\Http\Request;
use RLD\Http\Requests\UserRequest;
use JWTAuth;
use RLD\User;
use JWTAuthException;

class UserController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function register(UserRequest $request)
    {
        $count = User::count();
        if($count == 0) 
        {
            $user = $this->user->create([
              'name'        => $request->input('name'),
              'email'       => $request->input('email'),
              'password'    => bcrypt($request->input('password'))
            ]);
            return response()->json(['success'   => 'You can now login to your portfolio.'], 422);
        }
        else return response()->json(['system' => 'The system cannot accept registration.'], 422);
    }
    
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required|email|max:255',
            'password'  => 'required|max:255',
        ]);

        $credentials = $request->only('email', 'password');
        $token = null;

        try 
        {
           if (!$token = JWTAuth::attempt($credentials)) 
           {
            return response()->json(['credentials' => 'These credentials do not match our records.'], 422);
           }
        } 
        catch (JWTAuthException $e) 
        {
            return response()->json(['failed_to_create_token'], 500);
        }

        $user = JWTAuth::toUser($token);
        return response()->json(['data'=> $user, 'token'=> $token]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $data = User::where('id',$id)->get();
        return Response()->json(['data' => $data], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::where('id',$id)->get();
        return Response()->json(['data' => $data], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $base_path  = base_path().'/public';
        $old        = User::find($id);
        $unique     = ($old->email == $request->input('email')) ? '' : '|unique:users';
        
        $this->validate($request, [
            'email'                     => 'required|email|max:255.'.$unique.'.',
            'firstname'                 => 'required|max:100',
            'lastname'                  => 'required|max:100',
            'password'                  => 'required|max:100|confirmed',
            'password_confirmation'     => 'required|max:255',
        ]);


        $process = User::find($id);

        if($request->hasFile('photo'))
        {
            $valid_img = Validator::make($request->all(), ['photo' => 'image|mimes:png,jpg,jpeg,svg,gif']);
            if($valid_img->fails()) return Response()->json($valid_img->errors('photo'), 422);
            else
            {
                $db_image = str_replace(url('/'), $base_path, $process->image);
                if(file_exists($db_image)) unlink($db_image);

                $file           = $request->file('photo');
                $ext            = $file->guessClientExtension();
                $path           = base_path().'/public/uploads/user/';
                $image          = time().''.sha1(bcrypt(time())).'.n.'.$ext;
                $photo          = $request->file('photo')->move($path, $image);
                $public         = url('/').'/uploads/user/'.$image;
                $process->image = $public;
            }
        }

        $process->email        = $request->input('email');
        $process->firstname    = $request->input('firstname');
        $process->lastname     = $request->input('lastname');
        $process->password     = bcrypt($request->input('password'));
        
        $process->save();

        $user = User::find($id);

        if(!$process) return Response()->json(['failed' => 'Unable to process your post request'], 422);
        else return Response()->json(['success' => 'Account Successfully Updated', 'user' => $user], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace RLD\Http\Controllers;

use Illuminate\Http\Request;

use RLD\Http\Requests\TagRequest;

use RLD\Tag;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Tag::get();

        $new_ar = [];
        foreach ($data as $key => $value) 
        {
            $new_ar[$key]['value'] = $value['id'];
            $new_ar[$key]['label'] = $value['title'];
        }

        return Response()->json($new_ar, 200);
    }

    public function list(Request $request)
    {
        if($request->input('value') != '')
        {
            if($request->input('offset') == 0)
            {
                $data = Tag::where(
                    'id', 'like', '%'.$request->input('value').'%')
                    ->orWhere('title', 'like', '%'.$request->input('value').'%')
                    ->orWhere('status', 'like', '%'.$request->input('value').'%')
                    ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                    ->limit($request->input('per_page'))
                    ->orderBy('id', 'DESC')
                    ->get();
            }
            else
            {
                $data = Tag::where(
                    'id', 'like', '%'.$request->input('value').'%')
                    ->orWhere('title', 'like', '%'.$request->input('value').'%')
                    ->orWhere('status', 'like', '%'.$request->input('value').'%')
                    ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                    ->limit($request->input('per_page'))
                    ->offset($request->input('offset'))
                    ->orderBy('id', 'DESC')
                    ->get();
            }

            $count = count(Tag::where(
                'id', 'like', '%'.$request->input('value').'%')
                ->orWhere('title', 'like', '%'.$request->input('value').'%')
                ->orWhere('status', 'like', '%'.$request->input('value').'%')
                ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                ->orderBy('id', 'DESC')
                ->get()
            );
            
        }
        else
        {
            if($request->input('offset') == 0)
            {
                $data = Tag::limit($request->input('per_page'))
                    ->orderBy('id', 'DESC')
                    ->get();
            }
            else
            {
                $data = Tag::limit($request->input('per_page'))
                    ->offset($request->input('offset'))
                    ->orderBy('id', 'DESC')
                    ->get();
            }

            $count = count(Tag::get());
        }

        if(!$data) return Response()->json(['message', 'Error fetching records.'], 201);
        return Response()->json([['total' => $count, 'data' => $data]], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
        $process = new Tag;
        $process->title         = $request->input('title');
        $process->description   = $request->input('description');
        $process->status        = $request->input('status');
        $process->save();

        $id = $process->id;

        $data = Tag::find($id);

        if(!$process) return Response()->json(['failed' => 'Unable to process your post request'], 422);
        else return Response()->json(['success' => 'Successfully Added'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $json = Tag::find($id);
        if(!$json) return Response()->json(['message' => 'Unable to find item'], 201);
        return Response()->json([$json]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $json = Tag::find($id);
        if(!$json) return Response()->json(['message' => 'Unable to find item'], 201);
        return Response()->json([$json]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $process = Tag::find($id);
        $process->title         = $request->input('title');
        $process->description   = $request->input('description');
        $process->status        = $request->input('status');
        $process->save();

        if(!$process) return Response()->json(['failed' => 'Unable to process your post request'], 422);
        else return Response()->json(['success' => 'Successfully Updated'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $process = Tag::find($id);
        $process->delete();
        if($process) return Response()->json(['success' => 'Successfully Deleted'], 200);
        else return Response()->json(['error' => 'This data has been link to other data and cannot be destroy.'], 422);
    }
}

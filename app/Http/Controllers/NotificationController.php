<?php

namespace RLD\Http\Controllers;

use Illuminate\Http\Request;
use RLD\Notification,
    RLD\Visitor,
    RLD\Subscriber,
    RLD\Message;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->input('value') != '')
        {
            if($request->input('offset') == 0)
            {
                $data = Notification::where(
                    'id', 'like', '%'.$request->input('value').'%')
                    ->orWhere('title', 'like', '%'.$request->input('value').'%')
                    ->orWhere('type', 'like', '%'.$request->input('value').'%')
                    ->orWhere('status', 'like', '%'.$request->input('value').'%')
                    ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                    ->limit($request->input('per_page'))
                    ->orderBy('id', 'DESC')
                    ->get();
            }
            else
            {
                $data = Notification::where(
                    'id', 'like', '%'.$request->input('value').'%')
                    ->orWhere('title', 'like', '%'.$request->input('value').'%')
                    ->orWhere('type', 'like', '%'.$request->input('value').'%')
                    ->orWhere('status', 'like', '%'.$request->input('value').'%')
                    ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                    ->limit($request->input('per_page'))
                    ->offset($request->input('offset') + 1)
                    ->orderBy('id', 'DESC')
                    ->get();
            }

            $count = count(Notification::where(
                'id', 'like', '%'.$request->input('value').'%')
                ->orWhere('title', 'like', '%'.$request->input('value').'%')
                ->orWhere('type', 'like', '%'.$request->input('value').'%')
                ->orWhere('status', 'like', '%'.$request->input('value').'%')
                ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                ->orderBy('id', 'DESC')
                ->get()
            );
            
        }
        else
        {
            if($request->input('offset') == 0)
            {
                $data = Notification::limit($request->input('per_page'))
                    ->orderBy('id', 'DESC')
                    ->get();
            }
            else
            {
                $data = Notification::limit($request->input('per_page'))
                    ->offset($request->input('offset') + 1)
                    ->orderBy('id', 'DESC')
                    ->get();
            }

            $count = count(Notification::get());
        }

        $new = Notification::where('status',0)->count();

        if(!$data) return Response()->json(['message', 'Error fetching records.'], 201);
        return Response()->json([['total' => $count, 'data' => $data, 'new' => $new]], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($type, $id)
    {
        $data = [];

        switch ($type) {
            case 'visitor':
            case 'Visitor':
                $data = Visitor::find($id);
                break;
            case 'subsciber':
            case 'Subcriber':
                $data = Subcriber::find($id);
                break;
            default:
                $data = Message::find($id);
                break;
        }
        
        return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $process = Notification::find($id);
        $process->status = 1;
        $process->save();

        return response()->json(['message' => 'Notification #'.$id.' marked as read'], 200);
    }

    public function readAll(Request $request)
    {
        $process = Notification::where('status', '0')->update(['status' => 1]);
        return response()->json(['message' => 'Notifications marked as read.'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

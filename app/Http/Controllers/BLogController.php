<?php

namespace RLD\Http\Controllers;

use Illuminate\Http\Request;
use RLD\Http\Requests\BlogRequest;
use Illuminate\Support\Facades\Validator;

use RLD\Blog,
    RLD\Item_category, 
    RLD\Category,
    RLD\Item_tag,
    RLD\Tag,
    RLD\Jobs\SendArticleEmail;

use DB;

class BLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->input('value') != '')
        {
            if($request->input('offset') == 0)
            {
                $data = Blog::where(
                    'id', 'like', '%'.$request->input('value').'%')
                    ->orWhere('title', 'like', '%'.$request->input('value').'%')
                    ->orWhere('body', 'like', '%'.$request->input('value').'%')
                    ->orWhere('status', 'like', '%'.$request->input('value').'%')
                    ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                    ->limit($request->input('per_page'))
                    ->orderBy('id', 'DESC')
                    ->get();
            }
            else
            {
                $data = Blog::where(
                    'id', 'like', '%'.$request->input('value').'%')
                    ->orWhere('title', 'like', '%'.$request->input('value').'%')
                    ->orWhere('body', 'like', '%'.$request->input('value').'%')
                    ->orWhere('status', 'like', '%'.$request->input('value').'%')
                    ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                    ->limit($request->input('per_page'))
                    ->offset($request->input('offset'))
                    ->orderBy('id', 'DESC')
                    ->get();
            }

            $count = count(Blog::where(
                'id', 'like', '%'.$request->input('value').'%')
                ->orWhere('title', 'like', '%'.$request->input('value').'%')
                ->orWhere('body', 'like', '%'.$request->input('value').'%')
                ->orWhere('status', 'like', '%'.$request->input('value').'%')
                ->orWhere('created_at', 'like', '%'.$request->input('value').'%')
                ->orderBy('id', 'DESC')
                ->get()
            );
            
        }
        else
        {
            if($request->input('offset') == 0)
            {
                $data = Blog::limit($request->input('per_page'))
                    ->orderBy('id', 'DESC')
                    ->get();
            }
            else
            {
                $data = Blog::limit($request->input('per_page'))
                    ->offset($request->input('offset'))
                    ->orderBy('id', 'DESC')
                    ->get();
            }

            $count = count(Blog::get());
        }

        if(!$data) return Response()->json(['message', 'Error fetching records.'], 201);
        return Response()->json([['total' => $count, 'data' => $data]], 200);
    }

    public function recentPost() 
    {
        $data = Blog::limit(10)->orderBy('id', 'DESC')->get();
        return Response()->json($data);
    }

    public function categoryList() 
    {
        $data = Category::select('id','title')->orderBy('id', 'DESC')->get();
        return Response()->json($data);
    }


    public function postByCategory(Request $request, $id)
    {
        $data = DB::table('item_categories')
            ->join('blogs', 'item_categories.blog_id', '=', 'blogs.id')
            ->select('blogs.*', 'item_categories.blog_id', 'item_categories.category_id')
            ->where('item_categories.category_id', '=', $request->input('id'))
            ->where('item_categories.blog_id', '!=', 'NULL')
            ->limit($request->input('per_page'))
            ->offset($request->input('offset'))
            ->orderBy('id', 'DESC')
            ->get();

        $count = count(DB::table('item_categories')
            ->join('blogs', 'item_categories.blog_id', '=', 'blogs.id')
            ->select('blogs.*', 'item_categories.blog_id', 'item_categories.category_id')
            ->where('item_categories.category_id', '=', $request->input('id'))
            ->where('item_categories.blog_id', '!=', 'NULL')
            ->orderBy('id', 'DESC')
            ->get());

        return Response()->json([['total' => $count, 'data' => $data]], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        $process    = new Blog;

        $file       = $request->file('photo');
        $ext        = $file->guessClientExtension();
        $path       = base_path().'/public/uploads/blog/';
        $image      = time().''.sha1(bcrypt(time())).'.n.'.$ext;
        $photo      = $request->file('photo')->move($path, $image);
        $public     = url('/').'/uploads/blog/'.$image;

        $process->image         = $public;
        $process->title         = $request->input('title');
        $process->slug          = $request->input('slug');
        $process->body          = $request->input('body');
        $process->status        = $request->input('status');
        $process->save();

        $id = $process->id;

        $data = Blog::find($id);

        if(!$process) return Response()->json(['failed' => 'Unable to process your post request.'], 422);

        $categories = explode(',',$request->input('category'));
        foreach($categories as $keys)
        {
            $category               = new Item_category;
            $category->category_id  = $keys;
            $category->blog_id      = $id;
            $category->save();
        }

        $tags = [$request->input('tag')];
        foreach($tags as $keys)
        {
            $tag                    = new Item_tag;
            $tag->tag_id            = $keys;
            $tag->blog_id           = $id;
            $tag->save();
        }

        $job = (new SendArticleEmail($data));
        dispatch($job);

        return Response()->json(['success' => 'Successfully Added.'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $json = Blog::find($id)->toArray();
        if(!$json) return Response()->json(['message' => 'Unable to find item'], 201);

        $tags = DB::table('item_tags as it')
            ->select('t.title')
            ->join('tags as t', 't.id', '=', 'it.tag_id')
            ->where('it.blog_id', $id)
            ->get();

        $tagAr = [];
        foreach ($tags as $key => $value){ $tagAr[] = $value->title; };
        $word1 = implode(", ", $tagAr);

        $cats = DB::table('item_categories as ic')
            ->select('c.title')
            ->join('categories as c', 'c.id', '=', 'ic.category_id')
            ->where('ic.blog_id', $id)
            ->get();

        $catAr = [];
        foreach ($cats as $key => $value){ $catAr[] = $value->title; };
        $word2 = implode(", ", $catAr);

        $data = array_merge($json, ['tags' => $word1, 'categories' => $word2]);

        return Response()->json([$data]);
    }

    public function showBySlug($slug)
    {
        $json = Blog::where('slug',$slug)->get()->toArray();
        if(!$json) return Response()->json(['message' => 'Unable to find item'], 201);

        $tags = DB::table('item_tags as it')
            ->select('t.title')
            ->leftJoin('tags as t', 't.id', '=', 'it.tag_id')
            ->where('it.blog_id', $json[0]['id'])
            ->get();

        $tagAr = [];
        foreach ($tags as $key => $value){ $tagAr[] = $value->title; };
        $word1 = implode(", ", $tagAr);

        $cats = DB::table('item_categories as ic')
            ->select('c.title')
            ->join('categories as c', 'c.id', '=', 'ic.category_id')
            ->where('ic.blog_id', $json[0]['id'])
            ->get();

        $catAr = [];
        foreach ($cats as $key => $value){ $catAr[] = $value->title; };
        $word2 = implode(", ", $catAr);

        $data = array_merge($json[0], ['tags' => $word1, 'categories' => $word2]);

        return Response()->json([$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data       = Blog::find($id);
        $categories = Item_category::where('blog_id',$id)->get();
        $tags       = Item_tag::where('blog_id', $id)->get();

        $c_ar = [];
        foreach ($categories as $value) $c_ar[] = ''.$value['category_id'].'';
        $t_ar = [];
        foreach ($tags as $value) $t_ar[] = ''.$value['tag_id'].'';

        if(!$data) return Response()->json(['message' => 'Unable to find item'], 201);
        return Response()->json(['data' => $data, 'categories' => $c_ar, 'tags' => $t_ar],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $base_path              = base_path().'/public';
        $process                = Blog::find($id);

        $unique                 = '|unique:blogs';
        if($process->slug == $request->input('slug') || $process->title == $request->input('title'))
            $unique = '';
        
        $rules = [
            'title'         => 'required|max:255'.$unique.'',
            'slug'          => 'required|max:255'.$unique.'',
            'tag'           => 'required',
            'category'      => 'required',
            'body'          => 'required',
            'status'        => 'required|max:11',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) return Response()->json($validator->errors('title'), 422);

        $photo = '';

        if($request->hasFile('photo'))
        {
            $valid_img = Validator::make($request->all(), ['photo' => 'image|mimes:png,jpg,jpeg,svg,gif']);
            if($valid_img->fails()) return Response()->json($valid_img->errors('photo'), 422);
            else
            {
                $db_image = str_replace(url('/'), $base_path, $process->image);

                if(file_exists($db_image)) unlink($db_image);

                $file           = $request->file('photo');
                $ext            =  $file->guessClientExtension();
                $path           = base_path().'/public/uploads/blog/';
                $image          = time().''.sha1(bcrypt(time())).'.n.'.$ext;
                $photo          = $request->file('photo')->move($path, $image);
                $public         = url('/').'/uploads/blog/'.$image;
                $process->image = $public;
            }
        }


        $process->title         = $request->input('title');
        $process->slug          = $request->input('slug');
        $process->body          = $request->input('body');
        $process->status        = $request->input('status');
        $process->save();

        if(!$process) return Response()->json(['failed', 'Updating record failed.'], 201);
        else
        {
            Item_category::where('blog_id', $id)->delete();
            $categories = explode(',',$request->input('category'));

            foreach($categories as $keys)
            {
                $category               = new Item_category;
                $category->category_id  = $keys;
                $category->blog_id      = $id;
                $category->save();
            }

            Item_tag::where('blog_id', $id)->delete();
            $tags = explode(',',$request->input('tag'));

            foreach($tags as $keys)
            {
                $tag                    = new Item_tag;
                $tag->tag_id            = $keys;
                $tag->blog_id           = $id;
                $tag->save();
            }

            return Response()->json(['success' => 'Successfully Updated.'], 200);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $base_path              = base_path().'/public';
        $process = Blog::find($id);
        Item_category::where('blog_id',$id)->delete();
        Item_tag::where('blog_id',$id)->delete();

        $db_image = str_replace(url('/'), $base_path, $process->image);
        if(file_exists($db_image)) unlink($db_image);
        $process->delete();
        
        if(!$process) return Response()->json(['failed', 'Removing item error.'], 422);
        return Response()->json(['success' => 'Successfully Deleted.'], 200);
    }
}

<?php

namespace RLD\Http\Controllers;

use Illuminate\Http\Request;
use RLD\Visitor;
use RLD\Notification;
use RLD\Jobs\SendNotificationEmail;
use Stevebauman\Location\Position;

class VisitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $process            = new Visitor;
        $process->ip        = $request->getClientIp();
        $process->browser   = $request->header('User-Agent');
        $process->location  = json_encode(\Location::get($request->getClientIp()));
        $process->save();

        $id     = $process->id;
        $data   = Visitor::find($id);

        $process2           = new Notification;
        $process2->title    = 'You have a new website visitor';
        $process2->type     = 'visitor';
        $process2->type_id  = $id;
        $process2->save();

        return Response()->json(['data' => $data, 'message' => 'success'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

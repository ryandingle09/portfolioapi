<?php

namespace RLD\Http\Controllers;

use Illuminate\Http\Request;
use RLD\Http\Requests\siteRequest;
use RLD\Sitemeta;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo json_encode(Sitemeta::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(siteRequest $request)
    {
        $process = new Sitemeta;

        $process->title         = $request->input('title');
        $process->email         = $request->input('email');
        $process->tagline       = $request->input('tagline');
        $process->description   = $request->input('description');
        $process->contact       = $request->input('contact');
        $process->location      = $request->input('location');
        $process->owner         = $request->input('owner');

        if($reques->hasFile('photo'))
        {
            $file           = $request->file('photo');
            $ext            =  $file->guessClientExtension();
            $path           = base_path().'/public/uploads/site/';
            $image          = time().''.sha1(bcrypt(time())).'.n.'.$ext;
            $photo          = $request->file('photo')->move($path, $image);
            $public         = url('/').'/uploads/site/'.$image;
            $process->image = $public;
        }

        if($reques->hasFile('photo2'))
        {
            $file           = $request->file('photo2');
            $ext            = $file->guessClientExtension();
            $path           = base_path().'/public/uploads/site/';
            $image          = time().''.sha1(bcrypt(time())).'.n.'.$ext;
            $photo          = $request->file('photo2')->move($path, $image);
            $public         = url('/').'/uploads/site/'.$image;
            $process->logo  = $public;
        }

        $process->save();

        $data = Sitemeta::find($process->id());

        if($process) return Response()->json([$data]);
        else return Response()->json(['failed' => 'Failed to save data.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $base_path              = base_path().'/public';
        $process                = Sitemeta::find($id);

        $process->title         = $request->input('title');
        $process->email         = $request->input('email');
        $process->tagline       = $request->input('tagline');
        $process->description   = $request->input('description');
        $process->contact       = $request->input('contact');
        $process->location      = $request->input('location');
        $process->owner         = $request->input('owner');

        if($request->hasFile('photo')) 
        {
            $db_image       = str_replace(url('/'), $base_path, $process->image);

            if(file_exists($db_image)) unlink($db_image);

            $file           = $request->file('photo');
            $ext            =  $file->guessClientExtension();
            $path           = base_path().'/public/uploads/site/';
            $image          = time().''.sha1(bcrypt(time())).'.n.'.$ext;
            $photo          = $request->file('photo')->move($path, $image);
            $public         = url('/').'/uploads/site/'.$image;
            $process->image = $public;
        }

        if($request->hasFile('photo2')) 
        {
            $db_image       = str_replace(url('/'), $base_path, $process->image);

            if(file_exists($db_image)) unlink($db_image);

            $file           = $request->file('photo2');
            $ext            =  $file->guessClientExtension();
            $path           = base_path().'/public/uploads/site/';
            $image          = time().''.sha1(bcrypt(time())).'.n.'.$ext;
            $photo          = $request->file('photo2')->move($path, $image);
            $public         = url('/').'/uploads/site/'.$image;
            $process->logo  = $public;
        }
        
        $process->save();

        $data = Sitemeta::find($id);

        if($process) return Response()->json([$data]);
        else return Response()->json(['failed' => 'Failed to save data.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

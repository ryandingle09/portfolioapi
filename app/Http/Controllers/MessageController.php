<?php

namespace RLD\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use RLD\Message;
use RLD\Notification;
use RLD\Jobs\SendNotificationEmail;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required|max:255',
            'email'     => 'required|email|max:255',
            'message'   => 'required',
        ]);

        $process            = new Message;
        $process->name      = $request->input('name');
        $process->email     = $request->input('email');
        $process->message   = $request->input('message');
        $process->save();
        $id                 = $process->id;

        $data               = Message::find($id);

        $process2           = new Notification;
        $process2->title    = 'New message from '.$request->input('name').'';
        $process2->type     = 'message';
        $process2->type_id  = $id;
        $process2->save();

        $id2    = $process2->id;
        $notif  = Notification::findOrFail($id2);
        $job    = (new SendNotificationEmail($notif));

        dispatch($job);

        return Response()->json(['data' => $data, 'message' => 'success'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

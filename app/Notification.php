<?php

namespace RLD;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

    public function getCreatedAtAttribute($attr) {        
        return \Carbon\Carbon::parse($attr)->format('D, d M Y H:i:s O');
    }

	public function getUpdatedAtAttribute($attr)
	{
	   return \Carbon\Carbon::parse($attr)->format('D, d M Y H:i:s O');
	}
}

